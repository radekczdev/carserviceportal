package com.czajor.carserviceportal.service;

import com.czajor.carserviceportal.domain.CarDto;
import com.czajor.carserviceportal.exception.CarNotFoundException;
import com.czajor.carserviceportal.mapper.SameFieldsMapper;
import com.czajor.carserviceportal.model.Car;
import com.czajor.carserviceportal.repository.CarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.util.Optional.ofNullable;

@Service
public class CarService {

    private final Logger LOGGER = LoggerFactory.getLogger(Car.class);

    @Autowired
    private CarRepository carRepository;


    public List<Car> getCars() {
        return ofNullable(carRepository.findAll()).orElse(Collections.emptyList());
    }

    public Car getCar(final String carId) {
        return carRepository.findById(carId).orElse(new Car());
    }
    
    public Car addCar(final Car car) {
        return carRepository.save(car);
    }

    public void modifyCar(final CarDto carDto) {
        LOGGER.info("Starting to modifyCarParameters by CarService...");
        try {
            Car car = carRepository.findById(carDto.getLicensePlate()).orElseThrow(CarNotFoundException::new);
            car = (Car) SameFieldsMapper.map(car, carDto);
            carRepository.save(car);
        } catch (Exception e) {
            LOGGER.error("modifyCarParameters thrown message: " + e);
        }
    }

    public void deleteCar(final String licensePlate) {
        LOGGER.info("Starting to deleteCar by CarService...");
        try {
            carRepository.deleteById(licensePlate);
        } catch (EmptyResultDataAccessException e) {
            LOGGER.error("deleteCar thrown message: " + e.getMessage());
        }
    }
}
